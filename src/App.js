import React, {Suspense,useState,useEffect} from 'react';
import './App.css';
import {userInfo} from './endPoints';
const UserTable = React.lazy(() => import("./components/UserTable"));
const UserChart = React.lazy(() => import("./components/UserChart"));



function App() {
  const [userInfoState, getUserInfo] = useState([]);
  const [dataZero,getdataZero] = useState(false);
  useEffect (() =>{
      async function fetchUserAPI() {
          let response = await fetch(userInfo.url)
          response = await response.json()
          getUserInfo(response.results)
          getdataZero(true);
        }

        fetchUserAPI()
    },[]);
  return (
       <Suspense fallback={<div>Loading</div>}>
         {dataZero ?
          <>  
            <UserTable getusers = {userInfoState} /> 
            <UserChart getusers = {userInfoState} />
          </>
          : ''}
       </Suspense>
  );
}

export default App;

import React, {useState } from 'react';
import { MDBDataTableV5 } from 'mdbreact';

  
  const UserTable = ({getusers}) => {
	const [datatable] = useState({
  	 	columns :[
  	 			{ 
  	 				label: 'Name',
			        field: 'name',
			        width: 150,
			        attributes: {
			          'aria-controls': 'DataTable',
			          'aria-label': 'Name',
			        },
			      },
			      {
			        label: 'Location',
			        field: 'location',
			        width: 270,
			      },
			      {
			        label: 'Registered',
			        field: 'registered',
			        width: 200,
			      },
			      {
			        label: 'Phone',
			        field: 'phone',
			        sort: 'asc',
			        width: 100,
			      },
			      {
			        label: 'Picture',
			        field: 'picture',
			        sort: 'asc',
			        width: 100,
			      }, 
			      {
			        label: 'Action',
			        field: 'action',
			        width: 100,
			      },
			],
			 rows: getusers.map(items => {
 					return{
       					name: `${items.name.first} ${items.name.last}`,
       					location : `${items.location.city} ${items.location.country}`,
       					registered : Date.now(items.registered.date),
       					phone : items.phone,
       					picture:(
						          <img
						            src={items.picture.thumbnail}
						            alt ={items.name.first}
						          />),
       					action:(
						          <button
						          	 type='button'
						          	 onClick = {((e) =>e.preventDefault())}
						          >Delete</button>)

        			}
      		
      		})
    				
  	 });

  			
		return   <MDBDataTableV5 hover data={datatable} filter='name' proSelect searchTop searchBottom={false} />
} 

export default UserTable;
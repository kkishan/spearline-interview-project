import React, {useState } from 'react';
import { Pie } from "react-chartjs-2";
import { MDBContainer } from "mdbreact";

  
  const UserChart = ({getusers}) => {
		const [newZealandCount] = useState([]);
		const [Netherlands] = useState([]);
		const [Australia] = useState([]);
		const [Iran] = useState([]);
		const [Norway] = useState([]);
		getusers.filter((item,index)=>{
			if(item.location.country==='New Zealand') {
					newZealandCount.push(item.length);
			}
			else if(item.location.country==='Netherlands'){
					Netherlands.push(item.length);
			}
			else if(item.location.country==='Australia'){
					Australia.push(item.length);
			}
			else if(item.location.country==='Iran'){
					Iran.push(item.length);
			}
			else if(item.location.country==='Norway'){
					Norway.push(item.length);
			}else{
				return;	
			}
		})

  		const [state] = useState({
		    dataPie: {
		      labels: ["New Zealand", "Netherlands", "Australia", "Iran", "Norway"],
		      datasets: [
		        {
		          data: [newZealandCount.length, Netherlands.length, Australia.length, Iran.length, Norway.length],
		          backgroundColor: [
		            "#F7464A",
		            "#46BFBD",
		            "#FDB45C",
		            "#949FB1",
		            "#4D5360",
		            "#AC64AD"
		          ],
		          hoverBackgroundColor: [
		            "#FF5A5E",
		            "#5AD3D1",
		            "#FFC870",
		            "#A8B3C5",
		            "#616774",
		            "#DA92DB"
		          ]
		        }
		      ]
		    }
		  })
  		

		return (
				<MDBContainer>
		        <h3 className="mt-5">Pie chart</h3>
		        <Pie data={state.dataPie} options={{ responsive: true }} />
		      </MDBContainer>
		)  
} 

export default UserChart;